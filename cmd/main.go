package main

import (
	"callsRecorder/config"
	"callsRecorder/handlers"
	"callsRecorder/mediaStreamWriter"
	"callsRecorder/msgproc"
	"callsRecorder/peerRecorder"
	"callsRecorder/webrtcPeer"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"

	"runtime"
)

func init() {
	runtime.LockOSThread()
}

func main() {
	var conf config.RecorderConfig

	err := config.GetConfig(&conf)
	if err != nil {
		log.Fatalf("Failed to load config: %v", err)
	}

	composer := &msgproc.MessageComposer{}

	recorders := peerRecorder.NewRecorders()
	recorderFactory := peerRecorder.NewPeerRecorder(
		webrtcPeer.NewPeerConnection(conf.WebRTCConf),
		mediaStreamWriter.NewOpusStreamToFile)

	startRecordProc := msgproc.StartRecordProcessor(
		composer,
		recorders,
		recorderFactory,
	)
	stopRecordProc := msgproc.StopRecordProcessor(
		composer,
		recorders,
	)

	newMsgBuilder := func() msgproc.IMessageBuilder {
		return &msgproc.MessageBuilder{}
	}

	recCmdRouter, err := msgproc.NewRecorderMsgRouter(
		newMsgBuilder,
	)
	if err != nil {
		log.Fatalf("NewRecorderMsgRouter returned error: %v", err)
	}

	if err := recCmdRouter.Register("start_record", startRecordProc); err != nil {
		log.Fatalf("recCmdRouter.Register returned error: %v", err)
	}
	if err := recCmdRouter.Register("stop_record", stopRecordProc); err != nil {
		log.Fatalf("recCmdRouter.Register returned error: %v", err)
	}

	e := echo.New()
	e.Use(middleware.Logger())

	jwtGroup := e.Group("/jwt")

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
	}))

	jwtGroup.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey:    []byte(conf.Secret),
		SigningMethod: "HS256",
	}))

	jwtGroup.POST("/recorder", handlers.RecorderHandler(recCmdRouter))

	go func() {
		if err := e.Start(conf.HttpAddr); err != nil {
			log.Fatal(err)
		}
	}()
	mediaStreamWriter.StartMainLoop()
}
