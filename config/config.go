package config

import (
	"flag"
	"github.com/pion/webrtc/v2"
	"github.com/tkanos/gonfig"
)

type RecorderConfig struct {
	HttpAddr   string
	WebRTCConf webrtc.Configuration
	Secret     string
}

var flagStringVar func(p *string, name, value, usage string)

func init() {
	flagStringVar = flag.StringVar
}

// configPath returns path to config
// path is set in -c flag
// if -c flag is empty, "/" will be used
func configPath() (confPath string) {
	flagStringVar(&confPath, "c", "/", "path to a configuration file")
	flag.Parse()
	return
}

// GetConfig parse config information in config file to config struct
// Receives pointer to config struct
// Calls configPath func and gonfig.GetConf
func GetConfig(configType interface{}) error {
	return gonfig.GetConf(configPath(), configType)
}
