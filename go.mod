module callsRecorder

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/pion/rtcp v1.2.1
	github.com/pion/webrtc/v2 v2.1.18
	github.com/stretchr/testify v1.4.0
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
)
