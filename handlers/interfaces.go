package handlers

import (
	"golang.org/x/net/websocket"
	"net/http"
)

//go:generate mockery -name IUpgrader -case underscore -inpkg -testonly
// IUpgrader used for testing purpose
type IUpgrader interface {
	Upgrade(w http.ResponseWriter, r *http.Request, responseHeader http.Header) (*websocket.Conn, error)
}

//go:generate mockery -name IRecorderCmdRouter -case underscore -inpkg -testonly
type IRecorderCmdRouter interface {
	Process(jsonMsg []byte, claims map[string]interface{}) ([]byte, error)
}
