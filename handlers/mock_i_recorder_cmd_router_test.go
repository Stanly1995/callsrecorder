// Code generated by mockery v1.0.0. DO NOT EDIT.

package handlers

import mock "github.com/stretchr/testify/mock"

// MockIRecorderCmdRouter is an autogenerated mock type for the IRecorderCmdRouter type
type MockIRecorderCmdRouter struct {
	mock.Mock
}

// Process provides a mock function with given fields: jsonMsg, claims
func (_m *MockIRecorderCmdRouter) Process(jsonMsg []byte, claims map[string]interface{}) ([]byte, error) {
	ret := _m.Called(jsonMsg, claims)

	var r0 []byte
	if rf, ok := ret.Get(0).(func([]byte, map[string]interface{}) []byte); ok {
		r0 = rf(jsonMsg, claims)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func([]byte, map[string]interface{}) error); ok {
		r1 = rf(jsonMsg, claims)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
