package handlers

import (
	"callsRecorder/errtypes"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"io/ioutil"
	"net/http"
	"reflect"
)

// RecorderHandler gets claims from token, gets body and calls router.Process
// Used in calls recorder
func RecorderHandler(router IRecorderCmdRouter) func(ctx echo.Context) error {
	if router == nil || reflect.ValueOf(router).IsNil() {
		log.Panic(errtypes.ErrInvalidParam{}.AddParam("Router"))
	}
	return func(ctx echo.Context) (err error) {
		user := ctx.Get("user")
		token, ok := user.(*jwt.Token)
		if !ok {
			log.Error("invalid token")
			return echo.ErrBadRequest
		}
		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			log.Error("invalid claims")
			return echo.ErrBadRequest
		}
		reqBody, err := ioutil.ReadAll(ctx.Request().Body)
		if err != nil {
			return echo.ErrBadRequest
		}
		resp, err := router.Process(reqBody, claims)
		if err != nil {
			log.Error("process returned error: ", err.Error())
			return echo.ErrBadRequest
		}
		return ctx.Blob(http.StatusOK, "application/json", resp)
	}
}
