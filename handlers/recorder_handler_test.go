package handlers

import (
	"bytes"
	"callsRecorder/errtypes"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

type fakeClams struct{}

func (fc fakeClams) Valid() error {
	return nil
}

func TestRecorderHandler(t *testing.T) {
	// arrange
	cases := []struct {
		desc         string
		router       *MockIRecorderCmdRouter
		token        interface{}
		wantResp     string
		wantError    error
		wantStatus   int
		wantLog      string
		wantPanicLog string
	}{
		{
			desc:         "Should call log.Panic when IRouter is nil",
			wantPanicLog: errtypes.ErrInvalidParam{}.AddParam("Router").Error(),
		},
		{
			desc: "Should return 200 when router.Process returns nil error",
			router: func() *MockIRecorderCmdRouter {
				router := &MockIRecorderCmdRouter{}
				router.On("Process", []byte("request"), map[string]interface{}{
					"client_id": "user",
				}).Return([]byte("response"), nil).Once()
				return router
			}(),
			token: &jwt.Token{
				Raw:    "",
				Method: nil,
				Header: nil,
				Claims: jwt.MapClaims{
					"client_id": "user",
				},
				Signature: "",
				Valid:     false,
			},
			wantResp:   "response",
			wantStatus: http.StatusOK,
		},
		{
			desc:       "Should return 400 when invalid token",
			router:     &MockIRecorderCmdRouter{},
			token:      "invalid token",
			wantStatus: http.StatusBadRequest,
			wantLog:    "invalid token",
			wantError:  echo.ErrBadRequest,
		},
		{
			desc:   "Should return 400 when invalid claims",
			router: &MockIRecorderCmdRouter{},
			token: &jwt.Token{
				Raw:       "",
				Method:    nil,
				Header:    nil,
				Claims:    fakeClams{},
				Signature: "",
				Valid:     false,
			},
			wantStatus: http.StatusBadRequest,
			wantLog:    "invalid claims",
			wantError:  echo.ErrBadRequest,
		},
		{
			desc: "Should return 400 when router.Process returns error",
			router: func() *MockIRecorderCmdRouter {
				router := &MockIRecorderCmdRouter{}
				router.On("Process", []byte("request"), map[string]interface{}{
					"client_id": "user",
				}).Return([]byte{}, errors.New("processError")).Once()
				return router
			}(),
			token: &jwt.Token{
				Raw:    "",
				Method: nil,
				Header: nil,
				Claims: jwt.MapClaims{
					"client_id": "user",
				},
				Signature: "",
				Valid:     false,
			},
			wantStatus: http.StatusBadRequest,
			wantLog:    "processError",
			wantError:  echo.ErrBadRequest,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			if c.wantPanicLog != "" {
				assert.PanicsWithValue(t, c.wantPanicLog, func() { RecorderHandler(c.router) })
				return
			}
			// before
			var b bytes.Buffer
			log.SetOutput(&b)
			e := echo.New()
			req := httptest.NewRequest(http.MethodPost, "/", bytes.NewReader([]byte("request")))
			rec := httptest.NewRecorder()
			ctx := e.NewContext(req, rec)
			ctx.Set("user", c.token)
			router := c.router
			handler := RecorderHandler(router)
			// act
			gotError := handler(ctx)
			// assert
			assert.Equal(t, c.wantError, gotError)
			if gotError == nil {
				assert.Equal(t, c.wantStatus, rec.Code)
				assert.Equal(t, c.wantResp, rec.Body.String())
			}
			if c.wantLog == "" {
				assert.Zero(t, len(b.String()))
			} else {
				assert.Contains(t, b.String(), c.wantLog)
			}
			router.AssertExpectations(t)
		})
	}
}
