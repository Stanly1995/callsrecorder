package mediaStreamWriter

/*
#cgo pkg-config: gstreamer-1.0 gstreamer-app-1.0

#include "gst.h"

*/
import "C"
import (
	"callsRecorder/errtypes"
	"fmt"
	"io"
	"unsafe"
)

// StartMainLoop starts GLib's main loop
// It needs to be called from the process' main thread
// Because many gstreamer plugins require access to the main thread
// See: https://golang.org/pkg/runtime/#LockOSThread
func StartMainLoop() {
	C.gstreamer_receive_start_mainloop()
}

// OpusStreamsToFileWriter is a wrapper for a GStreamer Pipeline
type OpusStreamsToFileWriter struct {
	Pipeline *C.GstElement
}

// NewOpusStreamToFile creates a GStreamer Pipeline
func NewOpusStreamToFile(filename string) (io.WriteCloser, error) {
	if filename == "" {
		return nil, errtypes.ErrInvalidParam{}.AddParam("filename")
	}
	pipelineStr := fmt.Sprintf("appsrc format=time is-live=true do-timestamp=true name=src ! application/x-rtp, "+
		"payload=96, encoding-name=OPUS ! rtpopusdepay ! opusdec ! audioconvert ! lamemp3enc target=bitrate bitrate=12 ! id3v2mux ! filesink location=%s.mp3", filename)
	pipelineStrUnsafe := C.CString(pipelineStr)
	defer C.free(unsafe.Pointer(pipelineStrUnsafe))
	p := &OpusStreamsToFileWriter{Pipeline: C.gstreamer_receive_create_pipeline(pipelineStrUnsafe)}
	p.start()
	return p, nil
}

// Start starts the GStreamer Pipeline
func (p *OpusStreamsToFileWriter) start() {
	C.gstreamer_receive_start_pipeline(p.Pipeline)
}

// Close stops the GStreamer Pipeline
func (p *OpusStreamsToFileWriter) Close() error {
	C.gstreamer_receive_stop_pipeline(p.Pipeline)
	return nil
}

// Write pushes a buffer on the appsrc of the GStreamer Pipeline
func (p *OpusStreamsToFileWriter) Write(buffer []byte) (int, error) {
	b := C.CBytes(buffer)
	defer C.free(b)
	C.gstreamer_receive_push_buffer(p.Pipeline, b, C.int(len(buffer)))
	return 0, nil
}
