package msgproc

import (
	"callsRecorder/errtypes"
	"encoding/json"
)

// MessageComposer is used for input params serializing
type MessageComposer struct {
}

type message struct {
	Cmd  string      `json:"cmd"`
	Args interface{} `json:"args,omitempty"`
}

// Compose returns serialized object
func (c MessageComposer) Compose(cmd string, args interface{}) (data []byte, err error) {
	if cmd == "" {
		err = errtypes.ErrInvalidParam{}.AddParam("cmd")
		return
	}
	return json.Marshal(message{
		Cmd:  cmd,
		Args: args,
	})
}
