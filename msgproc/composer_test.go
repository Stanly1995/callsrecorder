package msgproc

import (
	"callsRecorder/errtypes"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMessageComposer_Compose(t *testing.T) {
	// arrange
	cases := []struct {
		desc         string
		composerType string
		cmd          string
		args         interface{}
		wantError    error
		wantBody     string
	}{
		{
			desc:         "Should return []byte and nil error",
			composerType: "json",
			cmd:          "someCMD",
			args:         StatusMsgArgs{Status:"ok"},
			wantError:    nil,
			wantBody:     `{"cmd":"someCMD","args":{"status":"ok"}}`,
		},
		{
			desc:         "Should return error when cmd is empty string",
			composerType: "json",
			cmd:          "",
			args:         "args",
			wantError:    errtypes.ErrInvalidParam{}.AddParam("cmd"),
		},
	}
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			composer := MessageComposer{}
			// act
			gotBody, gotError := composer.Compose(c.cmd, c.args)
			// assert
			assert.Equal(t, c.wantBody, string(gotBody))
			assert.Equal(t, c.wantError, gotError)

		})
	}
}
