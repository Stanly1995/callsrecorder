package msgproc

import "callsRecorder/peerRecorder"

//go:generate mockery -name IMessageComposer -case underscore -inpkg -testonly
type IMessageComposer interface {
	// Compose returns serialized object
	Compose(cmd string, args interface{}) ([]byte, error)
}

//go:generate mockery -name IRecorderGroup -case underscore -inpkg -testonly
type IRecorderGroup interface {
	Get(id string) peerRecorder.IPeerRecorder
	Set(id string, peerReq peerRecorder.IPeerRecorder) error
	Delete(id string)
}

//go:generate mockery -name IPeerRecorder -case underscore -inpkg -testonly
type IPeerRecorder interface {
	Start()
	GetAnswer() string
	Stop() error
	CloseCb(closeCb func()) error
}

//go:generate mockery -name IMessageParser -case underscore -inpkg -testonly
type IMessageParser interface {
	// ParseMsg deserializes input data to message field
	ParseMsg(data []byte) error
	// ParseArgs converts message args field to input interface
	ParseArgs(argsStruct interface{}) error
	// Cmd returns message cmd field
	Cmd() string
}

//go:generate mockery -name IMessageBuilder -case underscore -inpkg -testonly
type IMessageBuilder interface {
	IMessageParser
	SetClaims(map[string]interface{}) error
	ParseClaims(claimsStruct interface{}) error
}
