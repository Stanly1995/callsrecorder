package msgproc

import (
	"callsRecorder/errtypes"
	"github.com/mitchellh/mapstructure"
)

// MessageBuilder is wrapper for MessageParser
// It adds functional for work with claims
type MessageBuilder struct {
	MessageParser
	claims map[string]interface{}
}

// SetClaims put claims into MessageBuilder
func (mb *MessageBuilder) SetClaims(claims map[string]interface{}) error {
	if claims == nil {
		return errtypes.ErrInvalidParam{}.AddParam("claims")
	}
	mb.claims = claims
	return nil
}

// ParseClaims converts claims field to input interface
func (mb MessageBuilder) ParseClaims(claimsStruct interface{}) error {
	return mapstructure.Decode(mb.claims, claimsStruct)
}
