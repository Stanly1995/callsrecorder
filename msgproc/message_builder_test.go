package msgproc

import (
	"callsRecorder/errtypes"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMessageBuilder_SetClaims_Nil_Claims(t *testing.T) {
	// arrange
	mb := MessageBuilder{}
	var wantClaims map[string]interface{}
	// assert
	assert.Equal(t, errtypes.ErrInvalidParam{}.AddParam("claims"), mb.SetClaims(nil))
	assert.Equal(t, wantClaims, mb.claims)
}

func TestMessageBuilder_SetClaims(t *testing.T) {
	// arrange
	wantClaims := map[string]interface{}{
		"claim": "value",
	}
	mb := MessageBuilder{}
	// act
	assert.Nil(t, mb.SetClaims(wantClaims))
	// assert
	assert.Equal(t, wantClaims, mb.claims)
}

// Should return error when input param in ParseClaims func is invalid
func TestMessageBuilder_ParseClaims_Error(t *testing.T) {
	type invalidClaims struct {
		Claim int `mapstructure:"claim"`
	}
	// arrange
	mb := MessageBuilder{}
	inputClaims := map[string]interface{}{
		"claim": "value",
	}
	err := mb.SetClaims(inputClaims)
	if err != nil {
		t.Fail()
	}
	gotClaims := invalidClaims{}
	// assert
	assert.NotNil(t, mb.ParseClaims(&gotClaims))
	assert.Equal(t, invalidClaims{}, gotClaims)
}

// Should parse claims when input param in ParseClaims func is valid
func TestMessageBuilder_ParseClaims(t *testing.T) {
	type invalidClaims struct {
		Claim string `mapstructure:"claim"`
	}
	// arrange
	mb := MessageBuilder{}
	inputClaims := map[string]interface{}{
		"claim": "value",
	}
	err := mb.SetClaims(inputClaims)
	if err != nil {
		t.Fail()
	}
	gotClaims := invalidClaims{}
	// assert
	assert.Nil(t, mb.ParseClaims(&gotClaims))
	assert.Equal(t, invalidClaims{
		Claim: "value",
	}, gotClaims)
}
