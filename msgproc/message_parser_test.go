package msgproc

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMessageParser_Cmd(t *testing.T) {
	// arrange
	mp := &MessageParser{}
	mp.inputMessage.Cmd = "cmd"
	// assert
	assert.Equal(t, "cmd", mp.Cmd())
}

func TestMessageParser_ParseMsg(t *testing.T) {
	// arrange
	cases := []struct {
		desc        string
		data        []byte
		wantMessage inputMessage
		wantError   bool
	}{
		{
			desc: "Should return error when json library returns error",
			data: []byte(`invalid`),
			wantMessage: inputMessage{
				Cmd:  "",
				Args: nil,
			},
			wantError: true,
		},
		{
			desc: "Should return nil error when json library returns nil error",
			data: []byte(`{"cmd":"cmd","args":{"arg":"arg"}}`),
			wantMessage: inputMessage{
				Cmd: "cmd",
				Args: map[string]interface{}{
					"arg": "arg",
				}},
			wantError: false,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			mp := &MessageParser{}
			// act
			gotError := mp.ParseMsg(c.data)
			// assert
			if c.wantError {
				assert.NotNil(t, gotError)
			} else {
				assert.Nil(t, gotError)
			}
			assert.Equal(t, c.wantMessage, mp.inputMessage)
		})
	}
}

func TestMessageParser_ParseArgs(t *testing.T) {
	// arrange
	type wantArgs struct {
		Arg string
		Num int `mapstructure:"num"`
	}
	cases := []struct {
		desc      string
		mp        *MessageParser
		wantError bool
		wantArgs  wantArgs
	}{
		{
			desc: "Should parse args mapstructure library returns nil error",
			mp: &MessageParser{
				inputMessage{Args: map[string]interface{}{
					"arg": "arg",
					"num": 1,
				}},
			},
			wantError: false,
			wantArgs: wantArgs{
				Arg: "arg",
				Num: 1,
			},
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			var gotArgs wantArgs
			// act
			gotError := c.mp.ParseArgs(&gotArgs)
			// assert
			if c.wantError {
				assert.NotNil(t, gotError)
			} else {
				assert.Nil(t, gotError)
			}
			assert.Equal(t, c.wantArgs, gotArgs)
		})
	}
}
