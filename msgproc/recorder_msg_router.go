package msgproc

import (
	"callsRecorder/errtypes"
	"fmt"
)

type RecProcFunc func(inMsg IMessageBuilder) ([]byte, error)

var ErrInvalidProc = func(procName string) error {
	return fmt.Errorf("invalid message cmd was recieved: %s", procName)
}

// ErrProcExists is used when MsgRouter does not contain requested proc
var ErrProcExists = func(procName string) error {
	return fmt.Errorf("proc %s already registered", procName)
}

// ErrUnmarshal is used when builder.ParseMsg returns error
var ErrUnmarshal = func(err error) error {
	return fmt.Errorf("failed to unmarshal message: %v", err)
}

// Used in calls recorder for input requests. Is called in http handler.
type RecorderMsgRouter struct {
	newMsgBuilder func() IMessageBuilder
	procs         map[string]RecProcFunc
}

// NewRecorderMsgRouter creates RecorderMsgRouter
func NewRecorderMsgRouter(newMsgBuilder func() IMessageBuilder) (*RecorderMsgRouter, error) {
	if newMsgBuilder == nil {
		return nil, errtypes.ErrInvalidParam{}.AddParam("builder")
	}
	return &RecorderMsgRouter{
		newMsgBuilder: newMsgBuilder,
		procs:         map[string]RecProcFunc{},
	}, nil
}

// Register appends processor into internal processor map
func (mr *RecorderMsgRouter) Register(procName string, procFunc RecProcFunc) error {
	if procName == "" {
		return errtypes.ErrInvalidParam{}.AddParam("procName")
	}
	if procFunc == nil {
		return errtypes.ErrInvalidParam{}.AddParam("procFunc")
	}
	if _, ok := mr.procs[procName]; ok {
		return ErrProcExists(procName)
	}
	mr.procs[procName] = procFunc
	return nil
}

// Process parses managers' message
// and executes processor corresponding to a cmd field.
func (mr *RecorderMsgRouter) Process(jsonMsg []byte, claims map[string]interface{}) ([]byte, error) {
	builder := mr.newMsgBuilder()
	err := builder.ParseMsg(jsonMsg)
	if err != nil {
		return nil, ErrUnmarshal(err)
	}
	err = builder.SetClaims(claims)
	if err != nil {
		return nil, err
	}
	cmd := builder.Cmd()
	proc, ok := mr.procs[cmd]
	if !ok {
		return nil, ErrInvalidProc(cmd)
	}
	return proc(builder)
}
