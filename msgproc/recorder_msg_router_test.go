package msgproc

import (
	"callsRecorder/errtypes"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"reflect"
	"testing"
)

func TestNewRecorderMsgRouter(t *testing.T) {
	// arrange
	builder := &MockIMessageBuilder{}
	newMsgBuilder := func() IMessageBuilder {
		return builder
	}
	cases := []struct {
		desc         string
		newMsgParser func() IMessageBuilder
		wantRouter   *RecorderMsgRouter
		wantError    error
	}{
		{
			desc:         "Should return error when newMsgBuilder is nil",
			newMsgParser: nil,
			wantRouter:   nil,
			wantError:    errtypes.ErrInvalidParam{}.AddParam("builder"),
		},
		{
			desc:         "Should return *RecorderMsgRouter when newMsgBuilder is valid",
			newMsgParser: newMsgBuilder,
			wantRouter: &RecorderMsgRouter{
				newMsgBuilder: newMsgBuilder,
				procs:         map[string]RecProcFunc{},
			},
			wantError: nil,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// act
			gotRouter, gotError := NewRecorderMsgRouter(c.newMsgParser)
			// assert
			assert.Equal(t, c.wantError, gotError)
			if c.wantError != nil {
				assert.Equal(t, reflect.ValueOf(c.wantRouter), reflect.ValueOf(gotRouter))
			}
		})
	}
}

func TestRecorderMsgRouter_Register(t *testing.T) {
	// assert
	cases := []struct {
		desc      string
		procName  string
		procFunc  RecProcFunc
		wantError error
		before    func(mr *RecorderMsgRouter)
		after     func(mr *RecorderMsgRouter)
	}{
		{
			desc: "Should return error when procName param is empty",
			procFunc: func(inMsg IMessageBuilder) (bytes []byte, e error) {
				return
			},
			wantError: errtypes.ErrInvalidParam{}.AddParam("procName"),
		},
		{
			desc:      "Should return error when procFunc param is nil",
			procName:  "nil",
			wantError: errtypes.ErrInvalidParam{}.AddParam("procFunc"),
		},
		{
			desc: "Should return error when procName already exists",
			before: func(mr *RecorderMsgRouter) {
				mr.procs["nil"] = func(inMsg IMessageBuilder) (bytes []byte, e error) {
					return
				}
			},
			procFunc: func(inMsg IMessageBuilder) (bytes []byte, e error) {
				return
			},
			procName:  "nil",
			wantError: ErrProcExists("nil"),
		},
		{
			desc: "Should place func to corresponding key",
			procFunc: func(inMsg IMessageBuilder) (bytes []byte, e error) {
				return []byte("testBytes"), nil
			},
			procName: "test",
			after: func(mr *RecorderMsgRouter) {
				b, e := mr.procs["test"](&MockIMessageBuilder{})
				assert.Equal(t, []byte("testBytes"), b)
				assert.Nil(t, e)
			},
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// before
			mr, _ := NewRecorderMsgRouter(func() IMessageBuilder {
				return &MockIMessageBuilder{}
			})
			if c.before != nil {
				c.before(mr)
			}
			// act
			gotError := mr.Register(c.procName, c.procFunc)
			// assert
			if c.after != nil {
				c.after(mr)
			}
			assert.Equal(t, c.wantError, gotError)
		})
	}
}

func TestRecorderMsgRouter_Process(t *testing.T) {
	// arrange
	var msgBuilder MockIMessageBuilder
	newMsgBuilder := func() IMessageBuilder {
		return &msgBuilder
	}
	claims := map[string]interface{}{
		"claim": "value",
	}
	cases := []struct {
		desc              string
		recorderMsgRouter func() *RecorderMsgRouter
		jsonMsg           []byte
		claims            map[string]interface{}
		wantResponse      []byte
		wantError         error
	}{
		{
			desc: "Should return error when  builder.ParseMsg returns error",
			recorderMsgRouter: func() *RecorderMsgRouter {
				msgBuilder.Mock = mock.Mock{}
				msgBuilder.On("ParseMsg", mock.AnythingOfType("[]uint8")).Run(func(args mock.Arguments) {
					gotJson := string(args[0].([]byte))
					assert.Equal(t, `jsonMsg`, gotJson)
				}).Return(errors.New("parse error")).Once()
				return &RecorderMsgRouter{
					newMsgBuilder: newMsgBuilder,
				}
			},
			jsonMsg:      []byte(`jsonMsg`),
			wantResponse: nil,
			wantError:    ErrUnmarshal(errors.New("parse error")),
		},
		{
			desc: "Should return error when  builder.SetClaims returns error",
			recorderMsgRouter: func() *RecorderMsgRouter {
				msgBuilder.Mock = mock.Mock{}
				msgBuilder.On("ParseMsg", mock.AnythingOfType("[]uint8")).Run(func(args mock.Arguments) {
					gotJson := string(args[0].([]byte))
					assert.Equal(t, `jsonMsg`, gotJson)
				}).Return(nil).Once()
				msgBuilder.On("SetClaims", claims).Return(errors.New("set claims error")).Once()
				return &RecorderMsgRouter{
					newMsgBuilder: newMsgBuilder,
				}
			},
			jsonMsg:      []byte(`jsonMsg`),
			wantResponse: nil,
			claims:       claims,
			wantError:    errors.New("set claims error"),
		},
		{
			desc: "Should return error when msgBuilder returns invalid cmd",
			recorderMsgRouter: func() *RecorderMsgRouter {
				msgBuilder.Mock = mock.Mock{}
				msgBuilder.On("ParseMsg", mock.AnythingOfType("[]uint8")).Run(func(args mock.Arguments) {
					gotJson := string(args[0].([]byte))
					assert.Equal(t, `jsonMsg`, gotJson)
				}).Return(nil).Once()
				msgBuilder.On("SetClaims", claims).Return(nil).Once()
				msgBuilder.On("Cmd").Return("invalid_cmd").Once()
				return &RecorderMsgRouter{
					newMsgBuilder: newMsgBuilder,
					procs:         map[string]RecProcFunc{},
				}
			},
			jsonMsg:      []byte(`jsonMsg`),
			wantResponse: nil,
			claims:       claims,
			wantError:    ErrInvalidProc("invalid_cmd"),
		},
		{
			desc: "Should return error when processor returns error",
			recorderMsgRouter: func() *RecorderMsgRouter {
				msgBuilder.Mock = mock.Mock{}
				msgBuilder.On("ParseMsg", mock.AnythingOfType("[]uint8")).Run(func(args mock.Arguments) {
					gotJson := string(args[0].([]byte))
					assert.Equal(t, `jsonMsg`, gotJson)
				}).Return(nil).Once()
				msgBuilder.On("SetClaims", claims).Return(nil).Once()
				msgBuilder.On("Cmd").Return("valid_cmd").Once()
				return &RecorderMsgRouter{
					newMsgBuilder: newMsgBuilder,
					procs: map[string]RecProcFunc{
						"valid_cmd": func(inMsg IMessageBuilder) (bytes []byte, e error) {
							return nil, errors.New("processor returns error")
						},
					},
				}
			},
			jsonMsg:      []byte(`jsonMsg`),
			wantResponse: nil,
			claims:       claims,
			wantError:    errors.New("processor returns error"),
		},
		{
			desc: "Should return nil error when processor returns nil error",
			recorderMsgRouter: func() *RecorderMsgRouter {
				msgBuilder.Mock = mock.Mock{}
				msgBuilder.On("ParseMsg", mock.AnythingOfType("[]uint8")).Run(func(args mock.Arguments) {
					gotJson := string(args[0].([]byte))
					assert.Equal(t, `jsonMsg`, gotJson)
				}).Return(nil).Once()
				msgBuilder.On("SetClaims", claims).Return(nil).Once()
				msgBuilder.On("Cmd").Return("valid_cmd").Once()
				return &RecorderMsgRouter{
					newMsgBuilder: newMsgBuilder,
					procs: map[string]RecProcFunc{
						"valid_cmd": func(inMsg IMessageBuilder) (bytes []byte, e error) {
							return []byte("response"), nil
						},
					},
				}
			},
			jsonMsg:      []byte(`jsonMsg`),
			wantResponse: []byte("response"),
			claims:       claims,
			wantError:    nil,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// act
			gotResponse, gotError := c.recorderMsgRouter().Process(c.jsonMsg, c.claims)
			// assert
			assert.Equal(t, string(c.wantResponse), string(gotResponse))
			assert.Equal(t, c.wantError, gotError)
			msgBuilder.AssertExpectations(t)
		})
	}
}
