package msgproc

import (
	"callsRecorder/errtypes"
	"callsRecorder/peerRecorder"
	"errors"
	"github.com/labstack/gommon/log"
	"github.com/pion/webrtc/v2"
	"reflect"
	"strconv"
	"time"
)

// getMediaFileName returns the combined clientId and time strings.
// Example "5e469d4b-d128-4adb-9fa1-614af9f07b03_1564391901"
var getMediaFileName = func(s string) string {
	return s + "_" + strconv.FormatInt(time.Now().UTC().Unix(), 10)
}

// StartRecordProcessor returns func which runs record
// Returns error when data params isn`n correct
func StartRecordProcessor(
	composer IMessageComposer,
	recorders IRecorderGroup,
	newPeerRecorder func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error),
) func(IMessageBuilder) ([]byte, error) {
	if composer == nil || reflect.ValueOf(composer).IsNil() {
		log.Panic(errtypes.ErrInvalidParam{}.AddParam("composer"))
	}
	if recorders == nil || reflect.ValueOf(recorders).IsNil() {
		log.Panic(errtypes.ErrInvalidParam{}.AddParam("recorders"))
	}
	if newPeerRecorder == nil {
		log.Panic(errtypes.ErrInvalidParam{}.AddParam("newPeerRecorder"))
	}

	return func(msgBuilder IMessageBuilder) ([]byte, error) {
		if msgBuilder == nil || reflect.ValueOf(msgBuilder).IsNil() {
			return nil, errtypes.ErrInvalidParam{}.AddParam("msgBuilder")
		}

		var args StartRecordArgs
		err := msgBuilder.ParseArgs(&args)
		if err != nil {
			return nil, err
		}
		if args.Sdp == "" {
			return nil, errtypes.ErrInvalidParam{}.AddParam("args.Sdp")
		}

		var claims StartRecordClaims
		err = msgBuilder.ParseClaims(&claims)
		if err != nil {
			return nil, err
		}
		if claims.ClientID == "" {
			return nil, errtypes.ErrInvalidParam{}.AddParam("claims.ClientID")
		}

		fileName := getMediaFileName(claims.ClientID)

		if rec := recorders.Get(claims.ClientID); rec != nil {
			return nil, errors.New("recording wasn't started due to recording is already in progress")
		}

		recorder, err := newPeerRecorder(webrtc.SessionDescription{
			SDP:  args.Sdp,
			Type: webrtc.SDPTypeOffer,
		}, fileName)
		if err != nil {
			return nil, err
		}

		err = recorder.CloseCb(func() { recorders.Delete(claims.ClientID) })
		if err != nil {
			return nil, errors.New("recording wasn't started due to error in close callback adding")
		}

		err = recorders.Set(claims.ClientID, recorder)
		if err != nil {
			return nil, err
		}

		recorder.Start()

		return composer.Compose("start_record_response", StartRecordArgs{
			Sdp: recorder.GetAnswer(),
		})
	}
}
