package msgproc

import (
	"callsRecorder/errtypes"
	"callsRecorder/peerRecorder"
	"errors"
	"github.com/pion/webrtc/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

func init() {
	getMediaFileName = func(s string) string {
		return s + "_plus some time"
	}
}

func TestStartRecordProcessor_Panic(t *testing.T) {
	// arrange
	cases := []struct {
		desc            string
		composer        *MockIMessageComposer
		recorders       *MockIRecorderGroup
		newPeerRecorder func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error)
		wantLog         string
	}{
		{
			desc:      "Should call log.Panic when composer is nil",
			composer:  nil,
			recorders: &MockIRecorderGroup{},
			newPeerRecorder: func(webrtc.SessionDescription, string) (recorder peerRecorder.IPeerRecorder, e error) {
				return recorder, e
			},
			wantLog: errtypes.ErrInvalidParam{}.AddParam("composer").Error(),
		},
		{
			desc:      "Should call log.Panic when recorders is nil",
			composer:  &MockIMessageComposer{},
			recorders: nil,
			newPeerRecorder: func(webrtc.SessionDescription, string) (recorder peerRecorder.IPeerRecorder, e error) {
				return recorder, e
			},
			wantLog: errtypes.ErrInvalidParam{}.AddParam("recorders").Error(),
		},
		{
			desc:            "Should call log.Panic when newPeerRecorder is nil",
			composer:        &MockIMessageComposer{},
			recorders:       &MockIRecorderGroup{},
			newPeerRecorder: nil,
			wantLog:         errtypes.ErrInvalidParam{}.AddParam("newPeerRecorder").Error(),
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// assert
			assert.PanicsWithValue(t, c.wantLog, func() { StartRecordProcessor(c.composer, c.recorders, c.newPeerRecorder) })
		})
	}
}

func TestStartRecordProcessor_Return(t *testing.T) {
	// arrange
	composer := &MockIMessageComposer{}
	recorders := &MockIRecorderGroup{}
	newPeerRecorder := func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error) {
		return &peerRecorder.PeerRecorder{}, nil
	}

	// act
	f := StartRecordProcessor(composer, recorders, newPeerRecorder)

	// assert
	assert.NotNil(t, f)
	assert.IsType(t, func(builder IMessageBuilder) (b []byte, e error) {
		return
	}, f)
}

func TestStartRecordProcessor_Processor(t *testing.T) {
	cases := []struct {
		description     string
		composer        *MockIMessageComposer
		recorders       *MockIRecorderGroup
		msgBuilder      *MockIMessageBuilder
		newPeerRecorder func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error)
		wantBytes       []byte
		wantError       error
	}{
		{
			description: "Should return error when msgBuilder is nil",
			composer:    &MockIMessageComposer{},
			recorders:   &MockIRecorderGroup{},
			newPeerRecorder: func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error) {
				return nil, nil
			},
			msgBuilder: nil,
			wantBytes:  nil,
			wantError:  errtypes.ErrInvalidParam{}.AddParam("msgBuilder"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when msgBuilder.ParseArgs returns error",
			composer:    &MockIMessageComposer{},
			recorders:   &MockIRecorderGroup{},
			newPeerRecorder: func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error) {
				return nil, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(errors.New("ParseArgs error")).Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("ParseArgs error"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when args.Spd is empty",
			composer:    &MockIMessageComposer{},
			recorders:   &MockIRecorderGroup{},
			newPeerRecorder: func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error) {
				return nil, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args.Get(0).(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = ""
					}).
					Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errtypes.ErrInvalidParam{}.AddParam("args.Sdp"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when msgBuilder.ParseClaims returns error",
			composer:    &MockIMessageComposer{},
			recorders:   &MockIRecorderGroup{},
			newPeerRecorder: func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error) {
				return nil, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()

				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StartRecordClaims")).
					Return(errors.New("ParseClaims error")).
					Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("ParseClaims error"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when claims.clientID is empty",
			composer:    &MockIMessageComposer{},
			recorders:   &MockIRecorderGroup{},
			newPeerRecorder: func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error) {
				return nil, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()

				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StartRecordClaims")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordClaims)
						if !ok {
							t.Fail()
						}
						arg.ClientID = ""
					}).Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errtypes.ErrInvalidParam{}.AddParam("claims.ClientID"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when recorder is existed in recorders group",
			composer:    &MockIMessageComposer{},
			recorders: func() *MockIRecorderGroup {
				r := &MockIRecorderGroup{}
				r.On("Get", "id").
					Return(&MockIPeerRecorder{}).
					Once()
				return r
			}(),
			newPeerRecorder: func(webrtc.SessionDescription, string) (peerRecorder.IPeerRecorder, error) {
				return nil, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()

				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StartRecordClaims")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordClaims)
						if !ok {
							t.Fail()
						}
						arg.ClientID = "id"
					}).Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("recording wasn't started due to recording is already in progress"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when newPeerRecorder return error",
			composer:    &MockIMessageComposer{},
			recorders: func() *MockIRecorderGroup {
				r := &MockIRecorderGroup{}
				r.On("Get", "id").
					Return(nil).
					Once()
				return r
			}(),
			newPeerRecorder: func(w webrtc.SessionDescription, s string) (peerRecorder.IPeerRecorder, error) {
				assert.Equal(t, webrtc.SessionDescription{
					SDP:  "sdp",
					Type: webrtc.SDPTypeOffer,
				}, w)
				assert.Equal(t, "id_plus some time", s)
				return nil, errors.New("newPeerRecorder error")
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()

				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StartRecordClaims")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordClaims)
						if !ok {
							t.Fail()
						}
						arg.ClientID = "id"
					}).Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("newPeerRecorder error"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when recorder.CloseCb returns error",
			composer:    &MockIMessageComposer{},
			recorders: func() *MockIRecorderGroup {
				r := &MockIRecorderGroup{}
				r.On("Get", "id").
					Return(nil).
					Once()
				return r
			}(),
			newPeerRecorder: func(w webrtc.SessionDescription, s string) (peerRecorder.IPeerRecorder, error) {
				assert.Equal(t, webrtc.SessionDescription{
					SDP:  "sdp",
					Type: webrtc.SDPTypeOffer,
				}, w)
				assert.Equal(t, "id_plus some time", s)

				m := &MockIPeerRecorder{}
				m.On("CloseCb", mock.AnythingOfType("func()")).Return(errors.New("closeCb error")).Once()
				return m, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()

				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StartRecordClaims")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordClaims)
						if !ok {
							t.Fail()
						}
						arg.ClientID = "id"
					}).Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("recording wasn't started due to error in close callback adding"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when recorders.Set returns error",
			composer:    &MockIMessageComposer{},
			recorders: func() *MockIRecorderGroup {
				r := &MockIRecorderGroup{}
				r.On("Get", "id").
					Return(nil).
					Once()
				r.On("Set", "id", mock.Anything).
					Return(errors.New("recorders.Set error")).
					Once()
				return r
			}(),
			newPeerRecorder: func(w webrtc.SessionDescription, s string) (peerRecorder.IPeerRecorder, error) {
				assert.Equal(t, webrtc.SessionDescription{
					SDP:  "sdp",
					Type: webrtc.SDPTypeOffer,
				}, w)
				assert.Equal(t, "id_plus some time", s)

				m := &MockIPeerRecorder{}
				m.On("CloseCb", mock.AnythingOfType("func()")).Return(nil).Once()
				return m, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()

				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StartRecordClaims")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordClaims)
						if !ok {
							t.Fail()
						}
						arg.ClientID = "id"
					}).Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("recorders.Set error"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return error when composer.Compose returns error",
			composer: func() *MockIMessageComposer {
				c := &MockIMessageComposer{}
				c.On("Compose", "start_record_response", mock.AnythingOfType("msgproc.StartRecordArgs")).
					Return(nil, errors.New("composer.Compose error")).
					Run(func(args mock.Arguments) {
						arg, ok := args[1].(StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()
				return c
			}(),
			recorders: func() *MockIRecorderGroup {
				r := &MockIRecorderGroup{}
				r.On("Get", "id").
					Return(nil).
					Once()
				r.On("Set", "id", mock.AnythingOfType("*msgproc.MockIPeerRecorder")).
					Return(nil).Once()
				return r
			}(),
			newPeerRecorder: func(w webrtc.SessionDescription, s string) (peerRecorder.IPeerRecorder, error) {
				assert.Equal(t, webrtc.SessionDescription{
					SDP:  "sdp",
					Type: webrtc.SDPTypeOffer,
				}, w)
				assert.Equal(t, "id_plus some time", s)

				m := &MockIPeerRecorder{}
				m.On("CloseCb", mock.AnythingOfType("func()")).Return(nil).Once()
				m.On("Start").Return().Once()
				m.On("GetAnswer").Return("sdp").Once()
				return m, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()

				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StartRecordClaims")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordClaims)
						if !ok {
							t.Fail()
						}
						arg.ClientID = "id"
					}).Once()
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("composer.Compose error"),
		},

		///////////////////////////////////////////////////////////////////////////////////////////////////

		{
			description: "Should return nil error and slice of byte when StartRecordProcessor returns function, which works correctly",
			composer: func() *MockIMessageComposer {
				c := &MockIMessageComposer{}
				c.On("Compose", "start_record_response", mock.AnythingOfType("msgproc.StartRecordArgs")).
					Return([]byte("some bytes"), nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[1].(StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()
				return c
			}(),
			recorders: func() *MockIRecorderGroup {
				r := &MockIRecorderGroup{}
				r.On("Get", "id").
					Return(nil).
					Once()
				r.On("Set", "id", mock.AnythingOfType("*msgproc.MockIPeerRecorder")).
					Return(nil).Once()
				return r
			}(),
			newPeerRecorder: func(w webrtc.SessionDescription, s string) (peerRecorder.IPeerRecorder, error) {
				assert.Equal(t, webrtc.SessionDescription{
					SDP:  "sdp",
					Type: webrtc.SDPTypeOffer,
				}, w)
				assert.Equal(t, "id_plus some time", s)

				m := &MockIPeerRecorder{}
				m.On("CloseCb", mock.AnythingOfType("func()")).Return(nil).Once()
				m.On("Start").Return().Once()
				m.On("GetAnswer").Return("sdp").Once()
				return m, nil
			},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseArgs", mock.AnythingOfType("*msgproc.StartRecordArgs")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordArgs)
						if !ok {
							t.Fail()
						}
						arg.Sdp = "sdp"
					}).Once()

				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StartRecordClaims")).
					Return(nil).
					Run(func(args mock.Arguments) {
						arg, ok := args[0].(*StartRecordClaims)
						if !ok {
							t.Fail()
						}
						arg.ClientID = "id"
					}).Once()
				return mb
			}(),
			wantBytes: []byte("some bytes"),
			wantError: nil,
		},
	}

	for _, c := range cases {
		t.Run(c.description, func(t *testing.T) {
			// act
			gotBytes, gotError := StartRecordProcessor(c.composer, c.recorders, c.newPeerRecorder)(c.msgBuilder)

			// assert
			assert.Equal(t, c.wantBytes, gotBytes)
			assert.Equal(t, c.wantError, gotError)

			c.composer.AssertExpectations(t)
			c.recorders.AssertExpectations(t)
			if c.msgBuilder != nil {
				c.msgBuilder.AssertExpectations(t)
			}
		})
	}
}
