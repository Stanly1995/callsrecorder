package msgproc

import (
	"callsRecorder/errtypes"
	"fmt"
	"github.com/labstack/gommon/log"
	"reflect"
)

// StopRecordProcessor stops recording by client id
func StopRecordProcessor(
	composer IMessageComposer,
	recorders IRecorderGroup,
) func(IMessageBuilder) ([]byte, error) {
	if composer == nil || reflect.ValueOf(composer).IsNil() {
		log.Panic(errtypes.ErrInvalidParam{}.AddParam("composer"))
	}
	if recorders == nil || reflect.ValueOf(recorders).IsNil() {
		log.Panic(errtypes.ErrInvalidParam{}.AddParam("recorders"))
	}

	return func(msgBuilder IMessageBuilder) ([]byte, error) {
		if msgBuilder == nil || reflect.ValueOf(msgBuilder).IsNil() {
			return nil, errtypes.ErrInvalidParam{}.AddParam("msgBuilder")
		}

		var claims StopRecordClaims
		err := msgBuilder.ParseClaims(&claims)
		if err != nil {
			return nil, err
		}
		if claims.ClientID == "" {
			return nil, fmt.Errorf("recording wasn't stopped due to invalid client id")
		}

		recorder := recorders.Get(claims.ClientID)
		if recorder == nil {
			return nil, fmt.Errorf("recorder with name: %s isn't existed", claims.ClientID)
		}
		err = recorder.Stop()
		if err != nil {
			return nil, fmt.Errorf("recorder with name: %s hasn't been stopped due to error: %v", claims.ClientID, err)
		}

		return composer.Compose("stop_record_response", StatusMsgArgs{Status: "ok"})
	}
}
