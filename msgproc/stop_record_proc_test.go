package msgproc

import (
	"callsRecorder/errtypes"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

func TestStopRecordProcessor_Panics(t *testing.T) {
	// arrange
	cases := []struct {
		desc      string
		composer  *MockIMessageComposer
		recorders *MockIRecorderGroup
		wantLog   string
	}{
		{
			desc:      "Should call log.Panic when composer is nil",
			composer:  nil,
			recorders: &MockIRecorderGroup{},
			wantLog:   errtypes.ErrInvalidParam{}.AddParam("composer").Error(),
		},
		{
			desc:      "Should call log.Panic when recorders is nil",
			composer:  &MockIMessageComposer{},
			recorders: nil,
			wantLog:   errtypes.ErrInvalidParam{}.AddParam("recorders").Error(),
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// assert
			assert.PanicsWithValue(t, c.wantLog, func() { StopRecordProcessor(c.composer, c.recorders) })
		})
	}
}

func TestStopRecordProcessor(t *testing.T) {
	// arrange
	cases := []struct {
		desc       string
		composer   *MockIMessageComposer
		recorders  *MockIRecorderGroup
		msgBuilder *MockIMessageBuilder
		wantBytes  []byte
		wantError  error
	}{
		{
			desc:       "Should return error when msgBuilder is nil",
			composer:   &MockIMessageComposer{},
			recorders:  &MockIRecorderGroup{},
			msgBuilder: nil,
			wantBytes:  nil,
			wantError:  errtypes.ErrInvalidParam{}.AddParam("msgBuilder"),
		},
		{
			desc:      "Should return error when msgBuilder.ParseClaims returns error",
			composer:  &MockIMessageComposer{},
			recorders: &MockIRecorderGroup{},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StopRecordClaims")).Return(errors.New("parse claims error"))
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("parse claims error"),
		},
		{
			desc:      "Should return error when msgBuilder.ParseClaims returns error",
			composer:  &MockIMessageComposer{},
			recorders: &MockIRecorderGroup{},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StopRecordClaims")).Return(errors.New("parse claims error"))
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("parse claims error"),
		},
		{
			desc:      "Should return error when msgBuilder.ParseClaims returns empty client id",
			composer:  &MockIMessageComposer{},
			recorders: &MockIRecorderGroup{},
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StopRecordClaims")).Run(
					func(args mock.Arguments) {
						arg, ok := args[0].(*StopRecordClaims)
						if !ok {
							t.Fail()
							return
						}
						arg.ClientID = ""
					}).Return(nil)
				return mb
			}(),
			wantBytes: nil,
			wantError: fmt.Errorf("recording wasn't stopped due to invalid client id"),
		},
		{
			desc:     "Should return error when recorders.Get returns nil recorder",
			composer: &MockIMessageComposer{},
			recorders: func() *MockIRecorderGroup {
				recorders := &MockIRecorderGroup{}
				recorders.On("Get", "id").Return(func() (nilRecorder IPeerRecorder) {
					return
				}())
				return recorders
			}(),
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StopRecordClaims")).Run(
					func(args mock.Arguments) {
						arg, ok := args[0].(*StopRecordClaims)
						if !ok {
							t.Fail()
							return
						}
						arg.ClientID = "id"
					}).Return(nil)
				return mb
			}(),
			wantBytes: nil,
			wantError: fmt.Errorf("recorder with name: %s isn't existed", "id"),
		},
		{
			desc:     "Should return error when recorder.Stop returns error",
			composer: &MockIMessageComposer{},
			recorders: func() *MockIRecorderGroup {
				recorders := &MockIRecorderGroup{}
				recorders.On("Get", "id").Return(func() IPeerRecorder {
					recorder := &MockIPeerRecorder{}
					recorder.On("Stop").Return(errors.New("recorder.Stop error"))
					return recorder
				}())
				return recorders
			}(),
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StopRecordClaims")).Run(
					func(args mock.Arguments) {
						arg, ok := args[0].(*StopRecordClaims)
						if !ok {
							t.Fail()
							return
						}
						arg.ClientID = "id"
					}).Return(nil)
				return mb
			}(),
			wantBytes: nil,
			wantError: fmt.Errorf("recorder with name: %s hasn't been stopped due to error: %v", "id", errors.New("recorder.Stop error")),
		},
		{
			desc: "Should return error when composer.Compose returns error",
			composer: func() *MockIMessageComposer {
				composer := &MockIMessageComposer{}
				composer.On("Compose", "stop_record_response", StatusMsgArgs{Status: "ok"}).Return(nil, errors.New("composer's error"))
				return composer
			}(),
			recorders: func() *MockIRecorderGroup {
				recorders := &MockIRecorderGroup{}
				recorders.On("Get", "id").Return(func() IPeerRecorder {
					recorder := &MockIPeerRecorder{}
					recorder.On("Stop").Return(nil)
					return recorder
				}())
				return recorders
			}(),
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StopRecordClaims")).Run(
					func(args mock.Arguments) {
						arg, ok := args[0].(*StopRecordClaims)
						if !ok {
							t.Fail()
							return
						}
						arg.ClientID = "id"
					}).Return(nil)
				return mb
			}(),
			wantBytes: nil,
			wantError: errors.New("composer's error"),
		},
		{
			desc: "Should return response when composer.Compose returns nil error",
			composer: func() *MockIMessageComposer {
				composer := &MockIMessageComposer{}
				composer.On("Compose", "stop_record_response", StatusMsgArgs{Status: "ok"}).Return([]byte("resp"), nil)
				return composer
			}(),
			recorders: func() *MockIRecorderGroup {
				recorders := &MockIRecorderGroup{}
				recorders.On("Get", "id").Return(func() IPeerRecorder {
					recorder := &MockIPeerRecorder{}
					recorder.On("Stop").Return(nil)
					return recorder
				}())
				return recorders
			}(),
			msgBuilder: func() *MockIMessageBuilder {
				mb := &MockIMessageBuilder{}
				mb.On("ParseClaims", mock.AnythingOfType("*msgproc.StopRecordClaims")).Run(
					func(args mock.Arguments) {
						arg, ok := args[0].(*StopRecordClaims)
						if !ok {
							t.Fail()
							return
						}
						arg.ClientID = "id"
					}).Return(nil)
				return mb
			}(),
			wantBytes: []byte("resp"),
			wantError: nil,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// act
			gotBytes, gotError := StopRecordProcessor(c.composer, c.recorders)(c.msgBuilder)
			// assert
			assert.Equal(t, string(c.wantBytes), string(gotBytes))
			assert.Equal(t, c.wantError, gotError)
			c.composer.AssertExpectations(t)
			c.recorders.AssertExpectations(t)
			if c.msgBuilder != nil {
				c.msgBuilder.AssertExpectations(t)
			}
		})
	}
}
