package msgproc

type StartRecordClaims struct {
	ClientID string `json:"client_id" mapstructure:"client_id"`
}

type StopRecordClaims struct {
	ClientID string `json:"client_id" mapstructure:"client_id"`
}

type StartRecordArgs struct {
	Sdp string `json:"sdp" mapstructure:"sdp"`
}

type StopCallArgs struct {
	ClientID string `json:"client_id" mapstructure:"client_id"`
}

type StatusMsgArgs struct {
	Status string `json:"status" mapstructure:"status"`
}
