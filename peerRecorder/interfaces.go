package peerRecorder

import "io"

//go:generate mockery -name IPeerRecorder -case underscore -inpkg -testonly
type IPeerRecorder interface {
	Start()
	GetAnswer() string
	Stop() error
	CloseCb(closeCb func()) error
}

//go:generate mockery -name IPeerConn -case underscore  -inpkg -testonly
type IPeerConn interface {
	GetAnswer() string
	Read() ([]byte, error)
	Close() error
	CloseCb(closeCb func()) error
}

//go:generate mockery -name IMediaStreamWriter -case underscore  -inpkg -testonly
type IMediaStreamWriter interface {
	io.WriteCloser
}
