// Code generated by mockery v1.0.0. DO NOT EDIT.

package peerRecorder

import mock "github.com/stretchr/testify/mock"

// MockIPeerConn is an autogenerated mock type for the IPeerConn type
type MockIPeerConn struct {
	mock.Mock
}

// Close provides a mock function with given fields:
func (_m *MockIPeerConn) Close() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CloseCb provides a mock function with given fields: closeCb
func (_m *MockIPeerConn) CloseCb(closeCb func()) error {
	ret := _m.Called(closeCb)

	var r0 error
	if rf, ok := ret.Get(0).(func(func()) error); ok {
		r0 = rf(closeCb)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetAnswer provides a mock function with given fields:
func (_m *MockIPeerConn) GetAnswer() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// Read provides a mock function with given fields:
func (_m *MockIPeerConn) Read() ([]byte, error) {
	ret := _m.Called()

	var r0 []byte
	if rf, ok := ret.Get(0).(func() []byte); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
