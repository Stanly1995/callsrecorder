package peerRecorder

import (
	"callsRecorder/errtypes"
	"github.com/labstack/gommon/log"
	"github.com/pion/webrtc/v2"
	"io"
)

// PeerRecorder reads bytes buffer from peer connection and writes it to writeCloser
// When PeerRecorder closes it should close Peer connection and close writeCloser
type PeerRecorder struct {
	peer        IPeerConn
	writeCloser io.WriteCloser
}

// Initializes PeerRecorder's simple factory by incoming constructors
func NewPeerRecorder(
	newPeer func(remoteSdp webrtc.SessionDescription) (peer IPeerConn, err error),
	newWriteCloser func(filename string) (wc io.WriteCloser, err error),
) func(remoteSdp webrtc.SessionDescription, fileName string) (IPeerRecorder, error) {
	if newPeer == nil {
		log.Panic(errtypes.ErrInvalidParam{}.AddParam("newPeer"))
	}
	if newWriteCloser == nil {
		log.Panic(errtypes.ErrInvalidParam{}.AddParam("newWriteCloser"))
	}
	return func(remoteSdp webrtc.SessionDescription, fileName string) (IPeerRecorder, error) {
		if fileName == "" {
			return nil, errtypes.ErrInvalidParam{}.AddParam("filename")
		}
		peer, err := newPeer(remoteSdp)
		if err != nil {
			return nil, err
		}
		writeCloser, err := newWriteCloser(fileName)
		if err != nil {
			return nil, err
		}
		return &PeerRecorder{
			peer:        peer,
			writeCloser: writeCloser,
		}, nil
	}
}

// Start starts recording peer's rtp packets
func (pr *PeerRecorder) Start() {
	go func() {
		for {
			buf, err := pr.peer.Read()
			if err != nil {
				log.Error(err)
				break
			}
			_, err = pr.writeCloser.Write(buf)
			if err != nil {
				log.Error(err)
				break
			}
		}
		err := pr.writeCloser.Close()
		if err != nil {
			log.Error(err)
		}
	}()
}

// GetAnswer returns peer's SDP information
func (pr *PeerRecorder) GetAnswer() string {
	return pr.peer.GetAnswer()
}

// Stop stops recording
func (pr *PeerRecorder) Stop() error {
	return pr.peer.Close()
}

// CloseCb is setter for callback which is processed when PeerRecorder.peer closes
func (pr *PeerRecorder) CloseCb(closeCb func()) error {
	return pr.peer.CloseCb(closeCb)
}
