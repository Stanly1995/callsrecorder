package peerRecorder

import (
	"bytes"
	"callsRecorder/errtypes"
	"errors"
	"github.com/labstack/gommon/log"
	"github.com/pion/webrtc/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"io"
	"strings"
	"sync"
	"testing"
	"time"
)

func TestNewPeerRecorder_Panics(t *testing.T) {
	// arrange
	cases := []struct {
		desc           string
		newPeer        func(remoteSdp webrtc.SessionDescription) (peer IPeerConn, err error)
		newWriteCloser func(filename string) (wc io.WriteCloser, err error)
		wantLog        string
	}{
		{
			desc:    "Should log panic when neewPeer func is nil",
			newPeer: nil,
			wantLog: errtypes.ErrInvalidParam{}.AddParam("newPeer").Error(),
		},
		{
			desc: "Should log panic when newWriteCloser func is nil",
			newPeer: func(remoteSdp webrtc.SessionDescription) (peer IPeerConn, err error) {
				return nil, nil
			},
			newWriteCloser: nil,
			wantLog:        errtypes.ErrInvalidParam{}.AddParam("newWriteCloser").Error(),
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// assert
			assert.PanicsWithValue(t, c.wantLog, func() { NewPeerRecorder(c.newPeer, c.newWriteCloser) })
		})
	}
}

func TestNewPeerRecorder(t *testing.T) {
	// arrange
	cases := []struct {
		desc             string
		newPeer          func(remoteSdp webrtc.SessionDescription) (peer IPeerConn, err error)
		newWriteCloser   func(filename string) (wc io.WriteCloser, err error)
		remoteSdp        webrtc.SessionDescription
		fileName         string
		wantPeerRecorder IPeerRecorder
		wantError        error
	}{
		{
			desc: "Should return error when fileName is empty",
			newPeer: func(remoteSdp webrtc.SessionDescription) (peer IPeerConn, err error) {
				return nil, nil
			},
			newWriteCloser: func(filename string) (wc io.WriteCloser, err error) {
				return nil, nil
			},
			fileName:         "",
			wantPeerRecorder: nil,
			wantError:        errtypes.ErrInvalidParam{}.AddParam("filename"),
		},
		{
			desc: "Should return error when newPeer returns error",
			newPeer: func(remoteSdp webrtc.SessionDescription) (peer IPeerConn, err error) {
				return nil, errors.New("new peer error")
			},
			newWriteCloser: func(filename string) (wc io.WriteCloser, err error) {
				return nil, nil
			},
			fileName:         "file",
			remoteSdp:        webrtc.SessionDescription{},
			wantPeerRecorder: nil,
			wantError:        errors.New("new peer error"),
		},
		{
			desc: "Should return error when newWriteCloser returns error",
			newPeer: func(remoteSdp webrtc.SessionDescription) (peer IPeerConn, err error) {
				return &MockIPeerConn{}, nil
			},
			newWriteCloser: func(filename string) (wc io.WriteCloser, err error) {
				return nil, errors.New("new writeCloser error")
			},
			fileName:         "file",
			remoteSdp:        webrtc.SessionDescription{},
			wantPeerRecorder: nil,
			wantError:        errors.New("new writeCloser error"),
		},
		{
			desc: "Should return PeerRecorder when constructors return valid params",
			newPeer: func(remoteSdp webrtc.SessionDescription) (peer IPeerConn, err error) {
				return &MockIPeerConn{}, nil
			},
			newWriteCloser: func(filename string) (wc io.WriteCloser, err error) {
				return &MockIMediaStreamWriter{}, nil
			},
			fileName:  "file",
			remoteSdp: webrtc.SessionDescription{},
			wantPeerRecorder: &PeerRecorder{
				peer:        &MockIPeerConn{},
				writeCloser: &MockIMediaStreamWriter{},
			},
			wantError: nil,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// act
			gotPeerRecorder, gotError := NewPeerRecorder(c.newPeer, c.newWriteCloser)(c.remoteSdp, c.fileName)
			// assert
			assert.Equal(t, c.wantPeerRecorder, gotPeerRecorder)
			assert.Equal(t, c.wantError, gotError)
		})
	}
}

func TestPeerRecorder_GetAnswer(t *testing.T) {
	peer := &MockIPeerConn{}
	peer.On("GetAnswer").Return("answer")
	pr := &PeerRecorder{
		peer: peer,
	}
	assert.Equal(t, "answer", pr.GetAnswer())
}

func TestPeerRecorder_Stop(t *testing.T) {
	// arrange
	cases := []struct {
		desc      string
		peer      func() *MockIPeerConn
		wantError error
	}{
		{
			desc: "Should return error when peer.Close returns error",
			peer: func() *MockIPeerConn {
				peer := &MockIPeerConn{}
				peer.On("Close").Return(errors.New("close error")).Once()
				return peer
			},
			wantError: errors.New("close error"),
		},
		{
			desc: "Should return nil error when peer.Close returns nil error",
			peer: func() *MockIPeerConn {
				peer := &MockIPeerConn{}
				peer.On("Close").Return(nil).Once()
				return peer
			},
			wantError: nil,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// assert
			peer := c.peer()
			recorder := &PeerRecorder{
				peer: peer,
			}
			assert.Equal(t, c.wantError, recorder.Stop())
			peer.AssertExpectations(t)
		})
	}
}

func TestPeerRecorder_Start(t *testing.T) {
	// arrange
	wgAssert := sync.WaitGroup{}
	cases := []struct {
		desc        string
		peer        func() *MockIPeerConn
		writeCloser func() *MockIMediaStreamWriter
		wantLog     string
	}{
		{
			desc: "Should log error when peer.Read returns error",
			peer: func() *MockIPeerConn {
				peer := &MockIPeerConn{}
				peer.On("Read").Return(nil, errors.New("read error")).Once()
				return peer
			},
			writeCloser: func() *MockIMediaStreamWriter {
				writeCloser := &MockIMediaStreamWriter{}
				writeCloser.On("Close").Run(func(args mock.Arguments) {
					time.Sleep(100 * time.Millisecond)
					wgAssert.Done()
				}).Return(nil).Once()
				return writeCloser
			},
			wantLog: "read error",
		},
		{
			desc: "Should log error when peer.Read returns error",
			peer: func() *MockIPeerConn {
				peer := &MockIPeerConn{}
				peer.On("Read").Return([]byte{128, 111, 123}, nil).Once()
				return peer
			},
			writeCloser: func() *MockIMediaStreamWriter {
				writeCloser := &MockIMediaStreamWriter{}
				writeCloser.On("Write", mock.AnythingOfType("[]uint8")).Run(func(args mock.Arguments) {
				}).Return(0, errors.New("write error")).Once()
				writeCloser.On("Close").Run(func(args mock.Arguments) {
					time.Sleep(100 * time.Millisecond)
					wgAssert.Done()
				}).Return(nil).Once()
				return writeCloser
			},
			wantLog: "write error",
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// before
			var b bytes.Buffer
			log.SetOutput(&b)
			wgAssert.Add(1)
			// act
			peer := c.peer()
			writeCloser := c.writeCloser()
			recorder := &PeerRecorder{
				peer:        peer,
				writeCloser: writeCloser,
			}
			recorder.Start()
			// assert
			wgAssert.Wait()
			assert.True(t, strings.Contains(b.String(), c.wantLog))
			peer.AssertExpectations(t)
			writeCloser.AssertExpectations(t)
		})
	}
}

func TestPeerRecorder_CloseCb(t *testing.T) {
	// arrange
	cases := []struct {
		desc      string
		peer      func() *MockIPeerConn
		closeCb   func()
		wantError error
	}{
		{
			desc: "Should return error when peer.CloseCb returns error",
			peer: func() *MockIPeerConn {
				peer := &MockIPeerConn{}
				peer.On("CloseCb", mock.Anything).Return(errors.New("peer closeCb error")).Once()
				return peer
			},
			closeCb:   func() {},
			wantError: errors.New("peer closeCb error"),
		},
		{
			desc: "Should return nil error when peer.CloseCb returns nil error",
			peer: func() *MockIPeerConn {
				peer := &MockIPeerConn{}
				peer.On("CloseCb", mock.Anything).Return(nil).Once()
				return peer
			},
			closeCb:   func() {},
			wantError: nil,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			peer := c.peer()
			pr := &PeerRecorder{
				peer: peer,
			}
			// assert
			assert.Equal(t, c.wantError, pr.CloseCb(c.closeCb))
			peer.AssertExpectations(t)
		})
	}
}
