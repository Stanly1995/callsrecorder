package peerRecorder

import (
	"callsRecorder/errtypes"
	"reflect"
	"sync"
)

// Recorders is container for peerRecorders
type Recorders struct {
	pool map[string]IPeerRecorder
	mu   sync.Mutex
}

// Recorders container initializer
func NewRecorders() *Recorders {
	return &Recorders{
		pool: make(map[string]IPeerRecorder),
	}
}

// Get gets peer recorder by its id
func (r *Recorders) Get(id string) IPeerRecorder {
	r.mu.Lock()
	defer r.mu.Unlock()
	v, ok := r.pool[id]
	if !ok {
		return nil
	}
	return v
}

// Set puts new peerRecorder to pecorders map with key - input id
// If id is empty or peerRecorder is nil, it should return error
func (r *Recorders) Set(id string, peerReq IPeerRecorder) error {
	if id == "" {
		return errtypes.ErrInvalidParam{}.AddParam("id")
	}
	if peerReq == nil || reflect.ValueOf(peerReq).IsNil() {
		return errtypes.ErrInvalidParam{}.AddParam("peerReq")
	}
	r.mu.Lock()
	r.pool[id] = peerReq
	r.mu.Unlock()
	return nil
}

// Delete deletes peer recorder from recorders map by its id
func (r *Recorders) Delete(id string) {
	r.mu.Lock()
	delete(r.pool, id)
	r.mu.Unlock()
}
