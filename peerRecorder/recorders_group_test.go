package peerRecorder

import (
	"callsRecorder/errtypes"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

func TestNewRecorders(t *testing.T) {
	// arrange
	wantRecorders := &Recorders{
		pool: make(map[string]IPeerRecorder),
		mu:   sync.Mutex{},
	}

	// act
	gotRecorders := NewRecorders()

	// assert
	assert.Equal(t, wantRecorders, gotRecorders)
}

func TestRecorders_Set(t *testing.T) {
	// arrange
	cases := []struct {
		description   string
		id            string
		peerRecorder  *MockIPeerRecorder
		wantRecorders *Recorders
		wantError     error
	}{
		{
			description:  "Should returns error when id param is empty",
			id:           "",
			peerRecorder: &MockIPeerRecorder{},
			wantRecorders: &Recorders{
				pool: map[string]IPeerRecorder{},
			},
			wantError: errtypes.ErrInvalidParam{}.AddParam("id"),
		},
		{
			description:  "Should returns error when perReq param is nil",
			id:           "some id",
			peerRecorder: nil,
			wantRecorders: &Recorders{
				pool: map[string]IPeerRecorder{},
			},
			wantError: errtypes.ErrInvalidParam{}.AddParam("peerReq"),
		},
		{
			description:  "Should returns nil error when recorders.Set method works correct",
			id:           "some id",
			peerRecorder: &MockIPeerRecorder{},
			wantRecorders: &Recorders{
				pool: map[string]IPeerRecorder{"some id": &MockIPeerRecorder{}},
			},
			wantError: nil,
		},
	}

	for _, c := range cases {
		t.Run(c.description, func(t *testing.T) {
			// act
			recorders := NewRecorders()
			gotError := recorders.Set(c.id, c.peerRecorder)

			// assert
			assert.Equal(t, c.wantRecorders, recorders)
			assert.Equal(t, c.wantError, gotError)
		})
	}
}

func TestRecorders_Get(t *testing.T) {
	// arrange
	cases := []struct {
		description      string
		id               string
		wantPeerRecorder IPeerRecorder
		recorders        map[string]IPeerRecorder
	}{

		{
			description:      "Should return nil pointer when id param is empty",
			id:               "",
			wantPeerRecorder: nil,
			recorders:        map[string]IPeerRecorder{},
		},
		{
			description:      "Should return nil pointer when id doesn`t exist in pool of records",
			id:               "other id",
			wantPeerRecorder: &MockIPeerRecorder{},
			recorders:        map[string]IPeerRecorder{"other id": &MockIPeerRecorder{}},
		},
		{
			description:      "Should return MockIPeerRecorder pointer when id exist in pool of records",
			id:               "some id",
			wantPeerRecorder: &MockIPeerRecorder{},
			recorders:        map[string]IPeerRecorder{"some id": &MockIPeerRecorder{}},
		},
	}

	for _, c := range cases {
		t.Run(c.description, func(t *testing.T) {
			// before
			recorders := NewRecorders()
			recorders.pool = c.recorders

			// act
			gotPeerRecorder := recorders.Get(c.id)

			// assert
			assert.Equal(t, c.wantPeerRecorder, gotPeerRecorder)
		})
	}
}

func TestRecorders_Delete(t *testing.T) {
	// arrange
	cases := []struct {
		description   string
		id            string
		recorders     map[string]IPeerRecorder
		wantRecorders map[string]IPeerRecorder
	}{
		{
			description:   "Should delete value by key in recorders pool",
			id:            "some id",
			recorders:     map[string]IPeerRecorder{"some id": &MockIPeerRecorder{}},
			wantRecorders: map[string]IPeerRecorder{},
		},
	}

	for _, c := range cases {
		t.Run(c.description, func(t *testing.T) {
			// before
			recorders := NewRecorders()
			recorders.pool = c.recorders
			// act
			recorders.Delete(c.id)

			// assert
			assert.Equal(t, c.wantRecorders, recorders.pool)
		})
	}
}
