package webrtcPeer

// Used in WebRTCPeer's unit tests
var testSdp = `v=0
o=- 8425169860634363656 2 IN IP4 127.0.0.1
s=-
t=0 0
a=group:BUNDLE 0
a=msid-semantic: WMS PKkBXnFKKER1NV1z10FqZLVItJkSsEaYkzHt
m=audio 60616 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 110 112 113 126
c=IN IP4 195.24.157.182
a=rtcp:9 IN IP4 0.0.0.0
a=candidate:3035778456 1 udp 2113937151 6b7856c8-00a2-406d-bfaf-768a351a550f.local 60616 typ host generation 0 network-cost 999
a=candidate:2413254442 1 udp 2113939711 805089ad-a8be-494b-b03c-e5ca9f44a827.local 60617 typ host generation 0 network-cost 999
a=candidate:842163049 1 udp 1677729535 195.24.157.182 60616 typ srflx raddr 0.0.0.0 rport 0 generation 0 network-cost 999
a=ice-ufrag:KeMX
a=ice-pwd:0gn0C+j8wJpuTz5odB1f0zEm
a=ice-options:trickle
a=fingerprint:sha-256 C0:6B:54:26:6E:E5:45:81:93:B1:2B:02:64:C1:63:D0:69:E9:85:AE:09:03:7D:5F:D7:96:52:EA:BB:33:C7:7B
a=setup:actpass
a=mid:0
a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
a=extmap:2 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01
a=extmap:3 urn:ietf:params:rtp-hdrext:sdes:mid
a=extmap:4 urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id
a=extmap:5 urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id
a=sendrecv
a=msid:PKkBXnFKKER1NV1z10FqZLVItJkSsEaYkzHt a92451c8-f432-4ef6-b4cf-61d7eea42d1c
a=rtcp-mux
a=rtpmap:111 opus/48000/2
a=rtcp-fb:111 transport-cc
a=fmtp:111 minptime=10;useinbandfec=1
a=rtpmap:103 ISAC/16000
a=rtpmap:104 ISAC/32000
a=rtpmap:9 G722/8000
a=rtpmap:0 PCMU/8000
a=rtpmap:8 PCMA/8000
a=rtpmap:106 CN/32000
a=rtpmap:105 CN/16000
a=rtpmap:13 CN/8000
a=rtpmap:110 telephone-event/48000
a=rtpmap:112 telephone-event/32000
a=rtpmap:113 telephone-event/16000
a=rtpmap:126 telephone-event/8000
a=ssrc:1517731510 cname:Ur62Wtx2vmPSHwBN
a=ssrc:1517731510 msid:PKkBXnFKKER1NV1z10FqZLVItJkSsEaYkzHt a92451c8-f432-4ef6-b4cf-61d7eea42d1c
a=ssrc:1517731510 mslabel:PKkBXnFKKER1NV1z10FqZLVItJkSsEaYkzHt
a=ssrc:1517731510 label:a92451c8-f432-4ef6-b4cf-61d7eea42d1c
`
