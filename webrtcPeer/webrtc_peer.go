package webrtcPeer

import (
	"callsRecorder/errtypes"
	"callsRecorder/peerRecorder"
	"errors"
	"github.com/labstack/gommon/log"
	"github.com/pion/rtcp"
	"github.com/pion/webrtc/v2"
	"sync"
	"time"
)

var NilTrackError = errors.New("track is nil")

type waiter struct {
	isDone  bool
	isAdded bool
	sync.WaitGroup
	m sync.Mutex
}

func (w *waiter) Done() {
	w.m.Lock()
	if w.isAdded && !w.isDone {
		w.WaitGroup.Done()
		w.isDone = true
	}
	w.m.Unlock()
}

func (w *waiter) Add() {
	w.m.Lock()
	if !w.isAdded {
		w.WaitGroup.Add(1)
		w.isAdded = true
	}
	w.m.Unlock()
}

type WebRTCPeer struct {
	webrtcPeer   *webrtc.PeerConnection
	track        *webrtc.Track
	closeCb      func()
	trackWaiter  *waiter
	answerWaiter *waiter
}

var readTrack = func(track *webrtc.Track) ([]byte, error) {
	rtpPacket, err := track.ReadRTP()
	if err != nil {
		return nil, err
	}
	return rtpPacket.Raw, nil
}

// NewPeerConnection initializes peer connection, sets callbacks and sets local description
func NewPeerConnection(config webrtc.Configuration) func(remoteSdp webrtc.SessionDescription) (peerRecorder.IPeerConn, error) {
	return func(remoteSdp webrtc.SessionDescription) (peerRecorder.IPeerConn, error) {
		m := webrtc.MediaEngine{}
		m.RegisterCodec(webrtc.NewRTPOpusCodec(webrtc.DefaultPayloadTypeOpus, 48000))
		api := webrtc.NewAPI(webrtc.WithMediaEngine(m))
		webrtcPeer, err := api.NewPeerConnection(config)
		if err != nil {
			return nil, err
		}

		peer := &WebRTCPeer{
			webrtcPeer:   webrtcPeer,
			closeCb:      func() {},
			answerWaiter: &waiter{},
			trackWaiter:  &waiter{},
		}

		peer.answerWaiter.Add()
		peer.trackWaiter.Add()

		peer.webrtcPeer.OnTrack(peer.trackCb)
		peer.webrtcPeer.OnICECandidate(peer.iceCandidateCb)
		peer.webrtcPeer.OnICEConnectionStateChange(peer.iceConnStateChangeCb)

		_, err = webrtcPeer.AddTransceiver(webrtc.RTPCodecTypeAudio)
		if err != nil {
			return nil, err
		}
		err = webrtcPeer.SetRemoteDescription(remoteSdp)
		if err != nil {
			return nil, err
		}
		localSdp, err := webrtcPeer.CreateAnswer(nil)
		if err != nil {
			return nil, err
		}
		err = webrtcPeer.SetLocalDescription(localSdp)
		if err != nil {
			return nil, err
		}
		return peer, nil
	}
}

// Gets SDP field from local description
func (wrtcp *WebRTCPeer) GetAnswer() string {
	wrtcp.answerWaiter.Wait()
	return wrtcp.webrtcPeer.LocalDescription().SDP
}

// Read reads webrtc peer's track
// If peer connection has track it will being read by this func
func (wrtcp *WebRTCPeer) Read() ([]byte, error) {
	wrtcp.trackWaiter.Wait()
	if wrtcp.track == nil {
		return nil, NilTrackError
	}
	return readTrack(wrtcp.track)
}

func (wrtcp *WebRTCPeer) iceCandidateCb(candidate *webrtc.ICECandidate) {
	if candidate == nil {
		wrtcp.answerWaiter.Done()
	}
}

func (wrtcp *WebRTCPeer) trackCb(track *webrtc.Track, receiver *webrtc.RTPReceiver) {
	wrtcp.track = track
	wrtcp.trackWaiter.Done()

	ticker := time.NewTicker(time.Second * 3)
	for range ticker.C {
		errSend := wrtcp.webrtcPeer.WriteRTCP([]rtcp.Packet{&rtcp.PictureLossIndication{MediaSSRC: track.SSRC()}})
		if errSend != nil {
			log.Error(errSend)
			ticker.Stop()
			return
		}
	}
}

func (wrtcp *WebRTCPeer) iceConnStateChangeCb(state webrtc.ICEConnectionState) {
	if state == webrtc.ICEConnectionStateFailed ||
		state == webrtc.ICEConnectionStateClosed ||
		state == webrtc.ICEConnectionStateDisconnected {
		err := wrtcp.close()
		if err != nil {
			log.Error(err)
			return
		}
		log.Debug("webRTC peer has been closed")
	}
}

// Close closes webrtc peer connection
func (wrtcp *WebRTCPeer) Close() error {
	return wrtcp.close()
}

func (wrtcp *WebRTCPeer) close() error {
	wrtcp.doneWaiters()
	wrtcp.closeCb()
	return wrtcp.webrtcPeer.Close()
}

func (wrtcp *WebRTCPeer) doneWaiters() {
	wrtcp.answerWaiter.Done()
	wrtcp.trackWaiter.Done()
}

// CloseCb sets close callback to WebRTCPeer
func (wrtcp *WebRTCPeer) CloseCb(closeCb func()) error {
	if closeCb == nil {
		return errtypes.ErrInvalidParam{}.AddParam("closeCb")
	}
	wrtcp.closeCb = closeCb
	return nil
}
