package webrtcPeer

import (
	"callsRecorder/errtypes"
	"fmt"
	"reflect"
	"sync"
	"testing"
	"time"

	"github.com/pion/webrtc/v2"
	"github.com/stretchr/testify/assert"
)

// Peer connection should be closed and closeCb should be called when Close func is called
func TestWebRTCPeer_Close(t *testing.T) {
	// arrange
	peer, _ := webrtc.NewPeerConnection(webrtc.Configuration{})
	wgClose := sync.WaitGroup{}
	wgClose.Add(1)
	gotClose := false
	wrtcp := &WebRTCPeer{
		webrtcPeer: peer,
		closeCb: func() {
			gotClose = true
			wgClose.Done()
		},
		answerWaiter: &waiter{},
		trackWaiter:  &waiter{},
	}
	wrtcp.answerWaiter.Add()
	wrtcp.trackWaiter.Add()
	// assert
	assert.Equal(t, webrtc.PeerConnectionStateNew, wrtcp.webrtcPeer.ConnectionState())
	// act
	err := wrtcp.Close()
	// assert
	assert.Nil(t, err)
	assert.Equal(t, webrtc.PeerConnectionStateClosed, wrtcp.webrtcPeer.ConnectionState())
	wgClose.Wait()
	assert.True(t, gotClose)
}

// GetAnswer func should return answer when peer has all ice-candidates
func TestWebRTCPeer_GetAnswer(t *testing.T) {
	// arrange
	peer, _ := webrtc.NewPeerConnection(webrtc.Configuration{})
	err := peer.SetRemoteDescription(webrtc.SessionDescription{
		Type: webrtc.SDPTypeOffer,
		SDP:  testSdp,
	})
	if err != nil {
		t.Fail()
	}
	localDesc, _ := peer.CreateAnswer(nil)
	err = peer.SetLocalDescription(localDesc)
	assert.Nil(t, err)
	wrtcp := &WebRTCPeer{
		webrtcPeer:   peer,
		closeCb:      func() {},
		answerWaiter: &waiter{},
	}
	wrtcp.answerWaiter.Add()
	timeNow := time.Now()
	go func() {
		time.Sleep(2 * time.Second)
		wrtcp.iceCandidateCb(nil)
	}()
	// ast
	wrtcp.GetAnswer()
	// assert
	if time.Since(timeNow) < 2*time.Second {
		t.Fail()
	}
}

// Track read should return rtp packet's payload when it gets remote track
func TestWebRTCPeer_Read(t *testing.T) {
	// arrange
	readTrack = func(track *webrtc.Track) (buf []byte, e error) {
		return []byte{128, 111, 123, 143}, nil
	}
	wrtcp := &WebRTCPeer{
		closeCb:     func() {},
		trackWaiter: &waiter{},
	}
	wrtcp.trackWaiter.Add()
	timeNow := time.Now()
	go func() {
		time.Sleep(2 * time.Second)
		wrtcp.trackCb(&webrtc.Track{}, &webrtc.RTPReceiver{})
	}()
	// act
	_, gotError := wrtcp.Read()
	if time.Since(timeNow) < 2*time.Second {
		t.Fail()
	}
	// assert
	assert.Nil(t, gotError)
}

// Peer should close when ice connection state has been failed or closed or disconnected
func TestWebRTCPeer_StateCb(t *testing.T) {
	// arrange
	peer, _ := webrtc.NewPeerConnection(webrtc.Configuration{})
	wrtcp := &WebRTCPeer{
		webrtcPeer:   peer,
		closeCb:      func() {},
		answerWaiter: &waiter{},
		trackWaiter:  &waiter{},
	}
	wrtcp.answerWaiter.Add()
	wrtcp.trackWaiter.Add()
	// assert
	assert.Equal(t, webrtc.PeerConnectionStateNew, wrtcp.webrtcPeer.ConnectionState())
	// act
	wrtcp.iceConnStateChangeCb(webrtc.ICEConnectionStateClosed)
	// assert
	assert.Equal(t, webrtc.PeerConnectionStateClosed, wrtcp.webrtcPeer.ConnectionState())
}

// CloseCb func should return error when input callback is nil
func TestWebRTCPeer_CloseCb_error(t *testing.T) {
	// arrange
	wrtcp := &WebRTCPeer{}
	wantError := errtypes.ErrInvalidParam{}.AddParam("closeCb")
	// assert
	assert.Equal(t, wantError, wrtcp.CloseCb(nil))
}

// CloseCb func should return nil error when input callback is valid
func TestWebRTCPeer_CloseCb(t *testing.T) {
	// arrange
	wrtcp := &WebRTCPeer{}
	// assert
	var wantFunc = func() { fmt.Println("func") }
	assert.Equal(t, nil, wrtcp.CloseCb(wantFunc))
	assert.Equal(t, reflect.ValueOf(wrtcp.closeCb), reflect.ValueOf(wantFunc))
}

// Read() func should return error when trackWaiter.Done but track is nil
func TestWebRTCPeer_Read_nilTrack(t *testing.T) {
	// arrange
	wrtcp := &WebRTCPeer{
		trackWaiter: &waiter{},
	}
	// act
	gotBytes, gotError := wrtcp.Read()
	// assert
	assert.Nil(t, gotBytes)
	assert.Equal(t, NilTrackError, gotError)
}
